package com.example.rumy.todosystem.views.details;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.models.Todo;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodoDetailsFragment extends android.app.Fragment implements TodoDetailsContracts.View {

    private TodoDetailsContracts.Presenter mPresenter;

    @BindView(R.id.details_todo_title)
    TextView mTodoTextViewTitle;

    @BindView(R.id.details_todo_description)
    TextView mTodoTextViewDescription;

    @BindView(R.id.details_todo_button)
    Button mTodoButton;

    @Inject
    public TodoDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_todo_details, container, false);

        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
        mPresenter.loadTodo();
    }

    @Override
    public void showTodo(Todo todo) {
        mTodoTextViewTitle.setText(todo.getTitle());
        mTodoTextViewDescription.setText(todo.getDescription());

        buttonLogic(todo.getStatus());
    }

    @Override
    public void setPresenter(TodoDetailsContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void switchButton(Todo todo) {
        int res;
        if (todo.getStatus()) {
            res = R.string.DONE;
        } else {
            res = R.string.NOT_DONE;
        }
        mTodoButton.setText(res);
    }


    @Override
    public void showError(Throwable e) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick(R.id.details_todo_button)
    void onButtonClick() {
        try {
            mPresenter.changeStatus(mTodoButton.getText().toString());
        } catch (Exception e) {
            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void buttonLogic(boolean status) {
        if (status) {
            mTodoButton.setText(R.string.DONE);
        } else {
            mTodoButton.setText(R.string.NOT_DONE);
        }
    }
}
