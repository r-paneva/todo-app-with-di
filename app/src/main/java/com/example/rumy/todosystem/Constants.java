package com.example.rumy.todosystem;

public class Constants {
    public static final String BASE_SERVER_URL = "http://10.145.113.71:8080/api";
    public static final int TODO_TITLE_MIN_LENGTH = 3;
    public static final int TODO_TITLE_MAX_LENGTH = 50;
    public static final int TODO_DESC_MIN_LENGTH = 3;
    public static final int TODO_DESC_MAX_LENGTH = 1500;

}
