package com.example.rumy.todosystem.validators.base;

public interface Validator<T> {
    boolean isValid(T object);
}
