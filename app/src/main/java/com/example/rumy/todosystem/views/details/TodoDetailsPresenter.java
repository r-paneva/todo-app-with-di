package com.example.rumy.todosystem.views.details;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.async.base.SchedulerProvider;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.services.base.TodoService;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class TodoDetailsPresenter implements TodoDetailsContracts.Presenter {

    private final SchedulerProvider mSchedulerProvider;
    private final TodoService mTodoService;

    private TodoDetailsContracts.View mView;
    private int mTodoId;
    private Todo mTodo;

    @Inject
    public TodoDetailsPresenter(SchedulerProvider mSchedulerProvider, TodoService mTodoService) {
        this.mSchedulerProvider = mSchedulerProvider;
        this.mTodoService = mTodoService;
    }

    @Override
    public void subscribe(TodoDetailsContracts.View view) {
        mView = view;
    }

    @Override
    public void loadTodo() {
        mView.showLoading();
        Disposable observable = Observable
                .create((ObservableOnSubscribe<Todo>) emitter -> {
                    mTodo = mTodoService.getDetailsById(mTodoId);
                    emitter.onNext(mTodo);
                    emitter.onComplete();
                })
                .subscribeOn(mSchedulerProvider.background())
                .observeOn(mSchedulerProvider.ui())
                .doOnError(mView::showError)
                .subscribe(mView::showTodo);

    }

    @Override
    public void setTodoId(int id) {
        mTodoId = id;
    }

    @Override
    public void changeStatus(String status) throws Exception {
        if (status.charAt(0) == 'N') {
            mTodo.setStatus(true);
        } else {
            mTodo.setStatus(false);
        }

        Disposable observable = Observable
                .create((ObservableOnSubscribe<Todo>) emitter -> {
                    mTodoService.updateTodo(mTodoId, mTodo);
                    emitter.onNext(mTodo);
                    emitter.onComplete();
                })
                .subscribeOn(mSchedulerProvider.background())
                .observeOn(mSchedulerProvider.ui())
                .doOnError(mView::showError)
                .subscribe(mView::switchButton);
    }
}
