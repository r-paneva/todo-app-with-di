package com.example.rumy.todosystem.services;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.repositories.base.Repository;
import com.example.rumy.todosystem.services.base.TodoService;
import com.example.rumy.todosystem.validators.base.Validator;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class HttpTodoService implements TodoService {

    private final Repository<Todo> mTodoRepository;
    private final Validator<Todo> mTodoValidator;

    public HttpTodoService(Repository<Todo> mTodoRepository, Validator<Todo> mTodoValidator) {
        this.mTodoRepository = mTodoRepository;
        this.mTodoValidator = mTodoValidator;
    }

    @Override
    public List<Todo> getAllTodos() throws IOException {
        return mTodoRepository.getAll();
    }

    @Override
    public Todo getDetailsById(int id) throws Exception {
        return mTodoRepository.getById(id);
    }

    @Override
    public List<Todo> getFilteredTodos(String pattern) throws IOException {
        String patternToLowerCase = pattern.toLowerCase();

        return getAllTodos().stream()
                .filter(todo -> todo.getTitle().toLowerCase().contains(patternToLowerCase))
                .collect(Collectors.toList());
    }

    @Override
    public Todo createTodo(Todo todo) throws Exception {
        if (!mTodoValidator.isValid(todo)) {
            throw new IllegalArgumentException("Todo is invalid");
        }

        return mTodoRepository.add(todo);
    }

    @Override
    public void updateTodo(int id, Todo todo) throws IOException {
        mTodoRepository.update(id, todo);
    }
}
