package com.example.rumy.todosystem.diconfig.modules.fragments;

import com.example.rumy.todosystem.diconfig.ActivityScoped;
import com.example.rumy.todosystem.diconfig.FragmentScoped;
import com.example.rumy.todosystem.views.details.TodoDetailsContracts;
import com.example.rumy.todosystem.views.details.TodoDetailsFragment;
import com.example.rumy.todosystem.views.details.TodoDetailsPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TodoDetailsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract TodoDetailsFragment todoDetailsFragment();

    @ActivityScoped
    @Binds
    abstract TodoDetailsContracts.Presenter todoDetailsPresenter(TodoDetailsPresenter presenter);
}
