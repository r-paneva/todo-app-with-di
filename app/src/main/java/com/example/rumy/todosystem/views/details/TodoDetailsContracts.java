package com.example.rumy.todosystem.views.details;

import com.example.rumy.todosystem.models.Todo;

public interface TodoDetailsContracts {
    interface View {
        void showTodo(Todo todo);

        void setPresenter(Presenter presenter);

        void switchButton(Todo todo);

        void showError(Throwable e);

        void showLoading();

        void hideLoading();
    }

    interface Presenter {
        void subscribe(View view);

        void loadTodo();

        void setTodoId(int id);

        void changeStatus(String status) throws Exception;
    }

}
