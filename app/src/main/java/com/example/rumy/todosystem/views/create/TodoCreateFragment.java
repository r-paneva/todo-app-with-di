package com.example.rumy.todosystem.views.create;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.models.Todo;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TodoCreateFragment extends android.app.Fragment implements TodoCreateContracts.View {

    @BindView(R.id.et_input_title)
    TextInputLayout mTodoInputTitle;

    @BindView(R.id.et_input_description)
    TextInputLayout mTodoInputDescription;

    @BindView(R.id.btn_create_todo)
    Button mTodoInputButton;

    private TodoCreateContracts.Presenter mPresenter;
    private TodoCreateContracts.Navigator mNavigator;

    @Inject
    public TodoCreateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_todo_create, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @OnClick(R.id.btn_create_todo)
    public void onTodoSaveClicked() {
        String title = mTodoInputTitle.getEditText().getText().toString();
        String desc = mTodoInputDescription.getEditText().getText().toString();
        Todo todo = new Todo(title, desc, false);
        mPresenter.save(todo);
    }

    @Override
    public void setPresenter(TodoCreateContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToHome() {
        mNavigator.navigateToHome();
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
        return;
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(TodoCreateContracts.Navigator navigator) {
        mNavigator = navigator;
    }
}
