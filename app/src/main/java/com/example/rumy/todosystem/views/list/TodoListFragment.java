package com.example.rumy.todosystem.views.list;


import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.models.Todo;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;


public class TodoListFragment extends android.app.Fragment implements TodoListContracts.View, TodoAdapter.OnTodoClickListener {


    private TodoListContracts.Navigator mNavigator;

    @BindView(R.id.rv_todos)
    RecyclerView mTodosView;

    @BindView(R.id.loading)
    ProgressBar mLoadingView;

    @BindView(R.id.search)
    EditText mSearchBar;

    @Inject
    TodoAdapter mTodoAdapter;

    private TodoListContracts.Presenter mPresenter;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;

    @Inject
    public TodoListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        // Apply ButterKnife
        ButterKnife.bind(this, view);

        mTodoAdapter.setOnTodoClickListener(this);

        mTodosView.setAdapter(mTodoAdapter);
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);
        mTodosView.setLayoutManager(mStaggeredGridLayoutManager);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
        mPresenter.loadTodos();
    }


    @Override
    public void setPresenter(TodoListContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showTodos(List<Todo> todos) {
        mTodoAdapter.clear();
        mTodoAdapter.addAll(todos);
        mTodoAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyTodoList() {
        Toast t = Toast.makeText(getContext(), "No Todos", Toast.LENGTH_LONG);
        t.setGravity(Gravity.TOP | Gravity.CENTER, 0, 100);
        t.show();
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        mTodosView.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mTodosView.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showTodoDetails(Todo todo) {
        mNavigator.navigateWith(todo);
    }

    @OnTextChanged(R.id.search)
    public void onTextChanged() {
        String pattern = mSearchBar.getText().toString();
        mPresenter.filterTodos(pattern);
    }


    @Override
    public void onClick(Todo todo) {
        mPresenter.selectTodo(todo);
    }

    void setNavigator(TodoListContracts.Navigator navigator) {
        mNavigator = navigator;
    }
}
