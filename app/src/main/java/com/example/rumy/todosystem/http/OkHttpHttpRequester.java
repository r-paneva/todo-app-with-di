package com.example.rumy.todosystem.http;

import com.example.rumy.todosystem.http.base.HttpRequester;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpHttpRequester implements HttpRequester {

    public OkHttpHttpRequester() {
        // keep empty
    }

    @Override
    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();
        OkHttpClient client = new OkHttpClient();
        Response response = client.newCall(request)
                .execute();
        return response.body().string();
    }

    @Override
    public String post(String url, String body) throws IOException {
        RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json"),
                body
        );
        Request request = new Request.Builder()
                .post(requestBody)
                .url(url)
                .build();
        OkHttpClient client = new OkHttpClient();
        Response response = client.newCall(request)
                .execute();
        return response.body().string();
    }

    @Override
    public void put(String url, String body) throws IOException {
        RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json"),
                body
        );
        Request request = new Request.Builder()
                .put(requestBody)
                .url(url)
                .build();
        OkHttpClient client = new OkHttpClient();
        client.newCall(request)
                .execute();
    }
}
