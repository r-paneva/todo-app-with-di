package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.parsers.GsonJsonParser;
import com.example.rumy.todosystem.parsers.base.JsonParser;

import dagger.Module;
import dagger.Provides;

@Module
public class ParsersModule {
    @Provides
    public JsonParser<Todo> todoJsonParser() {
        return new GsonJsonParser<>(Todo.class, Todo[].class);
    }
}
