package com.example.rumy.todosystem.validators;

import com.example.rumy.todosystem.Constants;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.validators.base.Validator;

public class TodoValidator implements Validator<Todo> {
    @Override
    public boolean isValid(Todo object) {
        return object != null &&
                isTitleValid(object) &&
                isDescValid(object);
    }

    private boolean isDescValid(Todo object) {
        return object.getDescription().length() >= Constants.TODO_DESC_MIN_LENGTH &&
                object.getDescription().length() <= Constants.TODO_DESC_MAX_LENGTH;
    }

    private boolean isTitleValid(Todo object) {
        return object.getTitle().length() >= Constants.TODO_TITLE_MIN_LENGTH &&
                object.getTitle().length() <= Constants.TODO_TITLE_MAX_LENGTH;
    }

}
