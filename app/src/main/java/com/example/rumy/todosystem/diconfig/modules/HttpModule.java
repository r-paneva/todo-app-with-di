package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.Constants;
import com.example.rumy.todosystem.http.OkHttpHttpRequester;
import com.example.rumy.todosystem.http.base.HttpRequester;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class HttpModule {
    @Provides
    public HttpRequester httpRequester() {
        return new OkHttpHttpRequester();
    }

    @Provides
    @Named("baseServerUrl")
    public String baseServerUrl() {
        return Constants.BASE_SERVER_URL;
    }
}
