package com.example.rumy.todosystem.views.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.models.Todo;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoCards> {

    private List<Todo> mTodos;
    private OnTodoClickListener mOnTodoClickListener;

    @Inject
    public TodoAdapter() {
        mTodos = new LinkedList<>();
    }

    @NonNull
    @Override
    public TodoCards onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.todo_card, null);
        return new TodoCards(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoCards holder, int position) {
        holder.setOnClickListener(mOnTodoClickListener);
        holder.bind(mTodos.get(position));
    }

    @Override
    public int getItemCount() {
        return mTodos.size();
    }

    public Todo getItem(int position) {
        return mTodos.get(position);
    }

    public void clear() {
        mTodos.clear();
    }

    public void addAll(List<Todo> todos) {
        mTodos.addAll(todos);
    }

    public void setOnTodoClickListener(OnTodoClickListener onTodoClickListener) {
        this.mOnTodoClickListener = onTodoClickListener;
    }

    public class TodoCards extends RecyclerView.ViewHolder {

        @BindView(R.id.rv_todo_content)
        TextView mTodoContent;

        @BindView(R.id.rv_todo_title)
        TextView mTodoTitle;

        private OnTodoClickListener mOnClickListener;
        private Todo mTodo;

        public TodoCards(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Todo todo) {
            mTodoContent.setText(todo.getDescription());
            mTodoTitle.setText(todo.getTitle());
            mTodo = todo;
        }

        @OnClick
        public void onClick() {
            mOnClickListener.onClick(mTodo);
        }

        public void setOnClickListener(OnTodoClickListener onClickListener) {
            mOnClickListener = onClickListener;
        }

    }


    interface OnTodoClickListener {
        void onClick(Todo todo);
    }
}
