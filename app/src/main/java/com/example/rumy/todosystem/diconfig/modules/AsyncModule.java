package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.async.AsyncSchedulerProvider;
import com.example.rumy.todosystem.async.base.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AsyncModule {
    @Provides
    @Singleton
    public SchedulerProvider schedulerProvider() {
        return AsyncSchedulerProvider.getInstance();
    }
}
